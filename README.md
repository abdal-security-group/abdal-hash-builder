# Abdal Hash Builder

## Project Programmer
> Ebrahim Shafiei (EbraSha) - Prof.Shafiei@Gmail.com

## Made For 

Generate hash types with one click


**Requires**
> Visual Studio 2019 - Telerik WinForm - Chilkat - .NetFramework 4.*
>

 


Features

- Supports  30 hash algorithms
- Beautiful appearance
- No malware
- Has an installation package
- Very high speed


## ❤️ Donation
> USDT:      TXLasexoQTjKMoWarikkfYRYWWXtbaVadB

> bitcoin:   19LroTSwWcEBY2XjvgP6X4d6ECZ17U2XsK

> For Iranian People -> MellatBank : 6104-3378-5301-4247



## Reporting Issues

If you are facing a configuration issue or something is not working as you expected to be, please use the **Abdal.Group@Gmail.Com** or **Prof.Shafiei@Gmail.com** . Issues on GitLab are also welcomed.

## ⚠️ Legal disclaimer ⚠️

Usage of Abdal Hash Builder for attacking targets without prior mutual consent is illegal. It's the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program.

